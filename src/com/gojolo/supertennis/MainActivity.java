package com.gojolo.supertennis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ImageView play = (ImageView)findViewById(R.id.play);
		play.setOnClickListener(new View.OnClickListener(
				) {
			
			@Override
			public void onClick(View v) {
			 Intent  intent  = new  Intent (MainActivity.this,Tennis.class);
			 startActivity(intent);
				
			}
		});
	}
}
