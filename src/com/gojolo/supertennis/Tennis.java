package com.gojolo.supertennis;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Tennis extends Activity implements View.OnTouchListener{
	private int _xDelta;
	private int _yDelta;
	ImageView  tennis,heart1,heart2,heart3,heart4,heart5;
	ImageView levelo1,levelo2,levelo3,levelo4,levelo5,levelo6;
	TextView puan;
	ViewGroup _root;
	static int tlknm,tgknm;//tennis konumu
	int time=50000;
	static int yon=0;
	static int yukaricikti=0,asagiindi=0;
	static int compot1,compot2,compot3,compot4,compot5;
	static int level=1,levelhiz=2000;
	RelativeLayout rlt;
	static int rpst;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tennis);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		final int screenHeight = displaymetrics.heightPixels;
		final int screenWidth = displaymetrics.widthPixels;
		yon=0;
		rpst=screenWidth-50;
		yukaricikti=0; asagiindi=0;
	  level=1; levelhiz=2000;
		rlt = (RelativeLayout)findViewById(R.id.rlt);
		final RelativeLayout.LayoutParams puans= new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
			puan = (TextView)findViewById(R.id.puan);
			puans.setMargins((screenWidth/3)+(screenWidth/2),(screenHeight/40),0,0);
			puan.setLayoutParams(puans);
			  puan.setText(String.valueOf(yukaricikti));
		 final RelativeLayout.LayoutParams hearts1 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
			heart1 = new ImageView(this);
		     heart1.setImageResource(R.drawable.heart);
		     hearts1.setMargins((screenWidth/30),(screenHeight/40),0,0);
		     heart1.setLayoutParams(hearts1);
		     rlt.addView(heart1);
		     final RelativeLayout.LayoutParams hearts2 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		     heart2 = new ImageView(this);
		     heart2.setImageResource(R.drawable.heart);
		     hearts2.setMargins((screenWidth/30)+(screenWidth/30)+(screenWidth/30),(screenHeight/40),0,0);
		     heart2.setLayoutParams(hearts2);
		     rlt.addView(heart2);  
		     final RelativeLayout.LayoutParams hearts3 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		     heart3 = new ImageView(this);
		     heart3.setImageResource(R.drawable.heart);
		     hearts3.setMargins((screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30),(screenHeight/40),0,0);
		     heart3.setLayoutParams(hearts3);
		     rlt.addView(heart3); 
		     final RelativeLayout.LayoutParams hearts4 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		     heart4 = new ImageView(this);
		     heart4.setImageResource(R.drawable.heart);
		     hearts4.setMargins((screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30),(screenHeight/40),0,0);
		     heart4.setLayoutParams(hearts4);
		     rlt.addView(heart4); 
		     final RelativeLayout.LayoutParams hearts5 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		     heart5 = new ImageView(this);
		     heart5.setImageResource(R.drawable.heart);
		     hearts5.setMargins((screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30)+(screenWidth/30),(screenHeight/40),0,0);
		     heart5.setLayoutParams(hearts5);
		     rlt.addView(heart5);
				final RelativeLayout.LayoutParams levelcon1 = new RelativeLayout.LayoutParams((screenWidth/4),(screenHeight/6));
				 levelo1 = new ImageView(this);
			     levelo1.setImageResource(R.drawable.level1);
			     levelcon1.setMargins(0,100,0,0);
			     levelo1.setLayoutParams(levelcon1);
			     rlt.addView(levelo1);
					final RelativeLayout.LayoutParams levelcon2 = new RelativeLayout.LayoutParams((screenWidth/4),(screenHeight/6));
					 levelo2 = new ImageView(this);
				     levelo2.setImageResource(R.drawable.level2);
				     levelcon2.setMargins(0,100,0,0);
				     levelo2.setLayoutParams(levelcon2);
				     rlt.addView(levelo2);
						final RelativeLayout.LayoutParams levelcon3 = new RelativeLayout.LayoutParams((screenWidth/4),(screenHeight/6));
						 levelo3 = new ImageView(this);
					     levelo3.setImageResource(R.drawable.level3);
					     levelcon3.setMargins(0,100,0,0);
					     levelo3.setLayoutParams(levelcon2);
					     rlt.addView(levelo3);
							final RelativeLayout.LayoutParams levelcon4 = new RelativeLayout.LayoutParams((screenWidth/4),(screenHeight/6));
							 levelo4 = new ImageView(this);
						     levelo4.setImageResource(R.drawable.level4);
						     levelcon4.setMargins(0,100,0,0);
						     levelo4.setLayoutParams(levelcon3);
						     rlt.addView(levelo4);
								final RelativeLayout.LayoutParams levelcon5 = new RelativeLayout.LayoutParams((screenWidth/4),(screenHeight/6));
								 levelo5 = new ImageView(this);
							     levelo5.setImageResource(R.drawable.level5);
							     levelcon5.setMargins(0,100,0,0);
							     levelo5.setLayoutParams(levelcon1);
							     rlt.addView(levelo5);
									final RelativeLayout.LayoutParams levelcon6 = new RelativeLayout.LayoutParams((screenWidth/4),(screenHeight/6));
									 levelo6 = new ImageView(this);
								     levelo6.setImageResource(R.drawable.level6);
								     levelcon6.setMargins(0,100,0,0);
								     levelo6.setLayoutParams(levelcon1);
								     rlt.addView(levelo6);
		levelo1.setVisibility(View.GONE);	
		levelo2.setVisibility(View.GONE);
		levelo3.setVisibility(View.GONE);
		levelo4.setVisibility(View.GONE);
		levelo5.setVisibility(View.GONE);
		levelo6.setVisibility(View.GONE);
		final RelativeLayout.LayoutParams knm1 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		final RelativeLayout.LayoutParams knm2 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		final RelativeLayout.LayoutParams knm3 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		final RelativeLayout.LayoutParams knm4 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		final RelativeLayout.LayoutParams knm5 = new RelativeLayout.LayoutParams(screenWidth/15,screenWidth/15);
		tennis = (ImageView)findViewById(R.id.tennis);
		final ImageView ball1 = new ImageView(this);
	     ball1.setImageResource(R.drawable.ball);
	     knm1.setMargins(0,0,0,0);
	     ball1.setLayoutParams(knm1);
	     rlt.addView(ball1);
		final ImageView ball2 = new ImageView(this);
		ball2.setImageResource(R.drawable.ball);
	    knm2.setMargins((screenWidth/5),0,0,0);
		ball2.setLayoutParams(knm2);
		rlt.addView(ball2);
	    final ImageView ball3 = new ImageView(this);
	    ball3.setImageResource(R.drawable.ball);
	    knm3.setMargins((screenWidth/5)+(screenWidth/5),0,0,0);
	    ball3.setLayoutParams(knm3);
	    rlt.addView(ball3);
		final ImageView ball4 = new ImageView(this);
		ball4.setImageResource(R.drawable.ball);
		knm4.setMargins((screenWidth/5)+(screenWidth/5)+(screenWidth/5),0,0,0);
	    ball4.setLayoutParams(knm4);
		rlt.addView(ball4);
		final ImageView ball5 = new ImageView(this);
		ball5.setImageResource(R.drawable.ball);
		knm5.setMargins((screenWidth/5)+(screenWidth/5)+(screenWidth/5)+(screenWidth/5),0,0,0);
		ball5.setLayoutParams(knm5);
		rlt.addView(ball5);
		ball1.setVisibility(View.GONE);
		ball2.setVisibility(View.GONE);
		ball3.setVisibility(View.GONE);
		ball4.setVisibility(View.GONE);
		ball5.setVisibility(View.GONE);
		final	CountDownTimer cnt=   new CountDownTimer(time, 1000) {

			  public void onTick(long millisUntilFinished) {
				  if((millisUntilFinished/1000)==48)
				  { kazandi(); }
				  if((millisUntilFinished/1000)==40)
				  {  
				 Random rnt1=new Random();
				 compot1=rnt1.nextInt(screenWidth-50);
				ball1.setVisibility(View.VISIBLE);
	    		   RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)ball1.getLayoutParams();
	    		    params1.leftMargin =compot1;
	    		    ball1.setLayoutParams(params1);
				top1(ball1,0,(screenHeight/4)+(screenHeight/4)+(screenHeight/4),levelhiz);}
			 if((millisUntilFinished/1000)==37)
			 {	
				 Random rnt2=new Random();
				 compot2=rnt2.nextInt(screenWidth);
				 ball2.setVisibility(View.VISIBLE);
	    		   RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams)ball2.getLayoutParams();
	    		    params2.leftMargin =compot2;
	    		    ball2.setLayoutParams(params2);
				 top2(ball2,0,(screenHeight/4)+(screenHeight/4)+(screenHeight/4),levelhiz);
			  }
				 if((millisUntilFinished/1000)==35)
				 {	 
					 Random rnt3=new Random();
					 compot3=rnt3.nextInt(screenWidth-50);
					 ball3.setVisibility(View.VISIBLE);
		    		   RelativeLayout.LayoutParams params3 = (RelativeLayout.LayoutParams)ball3.getLayoutParams();
		    		    params3.leftMargin =compot3;
		    		    ball3.setLayoutParams(params3);
					 top3(ball3,0,(screenHeight/4)+(screenHeight/4)+(screenHeight/4),levelhiz);
			  }
		 if((millisUntilFinished/1000)==32)
		 {	 
			 Random rnt4=new Random();
			 compot4=rnt4.nextInt(screenWidth-50);
			 ball4.setVisibility(View.VISIBLE);
  		   RelativeLayout.LayoutParams params4 = (RelativeLayout.LayoutParams)ball4.getLayoutParams();
		    params4.leftMargin =compot4;
		    ball4.setLayoutParams(params4);
			 top4(ball4,0,(screenHeight/4)+(screenHeight/4)+(screenHeight/4),levelhiz);
	  }
	 if((millisUntilFinished/1000)==27)
	 {	
		 Random rnt5=new Random();
		 compot5=rnt5.nextInt(screenWidth-50);
		 ball5.setVisibility(View.VISIBLE);
		   RelativeLayout.LayoutParams params5 = (RelativeLayout.LayoutParams)ball5.getLayoutParams();
		    params5.leftMargin =compot5;
		    ball5.setLayoutParams(params5);
		 top5(ball5,0,(screenHeight/4)+(screenHeight/4)+(screenHeight/4),levelhiz);
    }
	 }
			  public void onFinish() {
			  }
			 }
			 .start();
   	    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((screenWidth/6),(screenHeight/5));
   	    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
	    layoutParams.leftMargin = 150;
	    layoutParams.bottomMargin =(screenHeight/50);
	    layoutParams.rightMargin = -250;
	    tennis.setLayoutParams(layoutParams);
	    tennis.setOnTouchListener(this);
		
	}
	public boolean onTouch(View view, MotionEvent event) {
	    final int X = (int) event.getRawX();
	    final int Y = (int) event.getRawY();
	    switch (event.getAction() & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
	            RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
	            _xDelta = X - lParams.leftMargin;
	            break;
	        case MotionEvent.ACTION_UP:
	            break;
	        case MotionEvent.ACTION_POINTER_DOWN:
	            break;
	        case MotionEvent.ACTION_POINTER_UP:
	            break;
	        case MotionEvent.ACTION_MOVE:
	            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
	            layoutParams.leftMargin = X - _xDelta;
	            if(layoutParams.leftMargin>=0&&layoutParams.leftMargin<=500)
	            {view.setLayoutParams(layoutParams);
	             tlknm=view.getLeft();
	             tgknm=view.getRight();
	            }
	            break;
	    }
	    return true;
	}
	public void top1(final View view1,final int x,final int y,final int hz)
	{
		final TranslateAnimation  anim1 = new TranslateAnimation( 0,x, 0,y);
	    anim1.setDuration(hz);
	    anim1.setFillEnabled(true);
	    view1.startAnimation(anim1);
	    anim1.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    		   view1.setAnimation(anim1);
		    		   if(tlknm<compot1+25&&compot1+25<tgknm)
		    		   {yukari1(view1,x,y,hz);}
		    		   else{
		    			asagi1(view1,x,y,hz);
		    		   }
    	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
 });
	}
	public void top2(final View view1,final int x,final int y,final int hz)
	{
		final TranslateAnimation  anim1 = new TranslateAnimation( 0,x, 0,y);
	    anim1.setDuration(hz);
	    anim1.setFillEnabled(true);
	    view1.startAnimation(anim1);
	    anim1.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    		   view1.setAnimation(anim1);
		    		   if(tlknm<compot2+25&&compot2+25<tgknm)
		    		   {yukari2(view1,x,y,hz);}
		    		   else{
		    			asagi2(view1,x,y,hz);
		    		   }
    	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
 });
	}
	public void top3(final View view1,final int x,final int y,final int hz)
	{
		final TranslateAnimation  anim1 = new TranslateAnimation( 0,x, 0,y);
	    anim1.setDuration(hz);
	    anim1.setFillEnabled(true);
	    view1.startAnimation(anim1);
	    anim1.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    		   view1.setAnimation(anim1);
		    		   if(tlknm<compot3+25&&compot3+25<tgknm)
		    		   {yukari3(view1,x,y,hz);}
		    		   else{
		    			asagi3(view1,x,y,hz);
		    		   }
    	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
 });
	}
	public void top4(final View view1,final int x,final int y,final int hz)
	{
		final TranslateAnimation  anim1 = new TranslateAnimation( 0,x, 0,y);
	    anim1.setDuration(hz);
	    anim1.setFillEnabled(true);
	    view1.startAnimation(anim1);
	    anim1.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    		   view1.setAnimation(anim1);
		    		   if(tlknm<compot4+25&&compot4+25<tgknm)
		    		   {yukari4(view1,x,y,hz);}
		    		   else{
		    			asagi4(view1,x,y,hz);
		    		   }
    	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
 });
	}
	public void top5(final View view1,final int x,final int y,final int hz)
	{
		final TranslateAnimation  anim1 = new TranslateAnimation( 0,x, 0,y);
	    anim1.setDuration(hz);
	    anim1.setFillEnabled(true);
	    view1.startAnimation(anim1);
	    anim1.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    		   view1.setAnimation(anim1);
		    		   if(tlknm<compot5+25&&compot5+25<tgknm)
		    		   {yukari5(view1,x,y,hz);}
		    		   else{
		    			asagi5(view1,x,y,hz);
		    		   }
    	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
 });
	}
 public void yukari1(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.vurdu);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,-y-500);
	    anim.setDuration(hz);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        yukaricikti++;
		    	        kazandi();
		    		    Random rnt1=new Random();
		    		    compot1=rnt1.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin = compot1;
		    		    view.setLayoutParams(params);
		    		    top1(view,0,y,hz);
 	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void yukari2(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.vurdu);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,-y-500);
	    anim.setDuration(hz);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        yukaricikti++;
		    	        kazandi();
		    		    Random rnt2=new Random();
		    		    compot2=rnt2.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot2;
		    		    view.setLayoutParams(params);
		    		    top2(view,0,y,hz);
 	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void yukari3(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.vurdu);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,-y-500);
	    anim.setDuration(hz);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        yukaricikti++;
		    	        kazandi();
		    		    Random rnt3=new Random();
		    		    compot3=rnt3.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot3;
		    		    view.setLayoutParams(params);
		    		    top3(view,0,y,hz);
 	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void yukari4(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.vurdu);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,-y-500);
	    anim.setDuration(hz);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        yukaricikti++;
		    	        kazandi();
		    		    Random rnt4=new Random();
		    		    compot4=rnt4.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot4;
		    		    view.setLayoutParams(params);
		    		    top4(view,0,y,hz);
 	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void yukari5(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.vurdu);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,-y-500);
	    anim.setDuration(hz);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        yukaricikti++;
		    	        kazandi();
		    		    Random rnt5=new Random();
		    		    compot5=rnt5.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin = compot5;
		    		    view.setLayoutParams(params);
		    		    top5(view,0,y,hz);
 	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void asagi1(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.cikti);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,y);
	    anim.setDuration(500);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        asagiindi++;
		    	        kaybetti();
		    		    Random rnt1=new Random();
		    		    compot1=rnt1.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot1;
		    		    view.setLayoutParams(params);
		    		    top1(view,0,y,hz);
	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void asagi2(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.cikti);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,y);
	    anim.setDuration(500);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        asagiindi++;
		    	        kaybetti();
		    		    Random rnt2=new Random();
		    		    compot2=rnt2.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot2;
		    		    view.setLayoutParams(params);
		    		    top2(view,0,y,hz);
	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void asagi3(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.cikti);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,y);
	    anim.setDuration(500);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        asagiindi++;
		    	        kaybetti();
		    		    Random rnt3=new Random();
		    		    compot3=rnt3.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot3;
		    		    view.setLayoutParams(params);
		    		    top3(view,0,y,hz);
	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void asagi4(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.cikti);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,y);
	    anim.setDuration(500);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        asagiindi++;
		    	        kaybetti();
		    		    Random rnt4=new Random();
		    		    compot4=rnt4.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot4;
		    		    view.setLayoutParams(params);
		    		    top4(view,0,y,hz);
	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void asagi5(final View view,final int x,final int y,final int hz)
 {
	 MediaPlayer a1= MediaPlayer.create(this,R.raw.cikti);
	 a1.setVolume(1.0f, 1.0f);
    a1.start();        
    a1.setOnCompletionListener(new OnCompletionListener() {
	    public void onCompletion(MediaPlayer mp) {
	        mp.release();

	    };
	});
	   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
	    params.topMargin += (y);
	    params.leftMargin += (x);
	    view.setLayoutParams(params);
		TranslateAnimation  anim = new TranslateAnimation( 0,x, 0,y);
	    anim.setDuration(500);
	    anim.setFillAfter( true );
	    view.startAnimation(anim);
	    anim.setAnimationListener(new AnimationListener() {
	    	   @Override
	    	   public void onAnimationStart(Animation arg0) {
	
	    	   }
	    		   
		    	   @Override
		    	   public void onAnimationEnd(Animation arg0) {
		    	        asagiindi++;
		    	        kaybetti();
		    		    Random rnt5=new Random();
		    		    compot5=rnt5.nextInt(rpst);
		    		   RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
		    		    params.topMargin -= (y);
		    		    params.leftMargin =compot5;
		    		    view.setLayoutParams(params);
		    		    top5(view,0,y,hz);
	   }
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
});
 }
 public void kaybetti()
 {
	     if(asagiindi==1)
		 {
			heart5.setVisibility(View.GONE);
		 }
	     if(asagiindi==2)
		 {
			heart4.setVisibility(View.GONE);
		 }
	     if(asagiindi==3)
		 {
			heart3.setVisibility(View.GONE);
		 }
	     if(asagiindi==4)
		 {
			heart2.setVisibility(View.GONE);
		 }
	 if(asagiindi==5)
	 {
		 heart1.setVisibility(View.GONE);
		Intent intent  = new Intent(Tennis.this,GameOver.class);
		intent.putExtra("score",yukaricikti);
			startActivity(intent);
	 }
 }
 public void kazandi()
 {
	 puan.setText(String.valueOf(yukaricikti));
   if(yukaricikti==0)
   {
	   level++;
	   levelo1.setVisibility(View.VISIBLE);
	     TranslateAnimation  anim = new TranslateAnimation(0,800, 0,0);
		    anim.setDuration(4000);
		    anim.setFillAfter( true );
		    levelo1.startAnimation(anim);
		    anim.setAnimationListener(new AnimationListener() {
		    	   @Override
		    	   public void onAnimationStart(Animation arg0) {
		
		    	   }
		    		   
			    	   @Override
			    	   public void onAnimationEnd(Animation arg0) {
			    		   levelo1.setVisibility(View.GONE);
		   }
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
	});
		
   }
   if(yukaricikti==50)
   {
	   level++;
	   levelo2.setVisibility(View.VISIBLE);
	     TranslateAnimation  anim = new TranslateAnimation(0,800, 0,0);
		    anim.setDuration(4000);
		    anim.setFillAfter( true );
		    levelo2.startAnimation(anim);
		    anim.setAnimationListener(new AnimationListener() {
		    	   @Override
		    	   public void onAnimationStart(Animation arg0) {
		
		    	   }
		    		   
			    	   @Override
			    	   public void onAnimationEnd(Animation arg0) {
			    		   levelo2.setVisibility(View.GONE);
		   }
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
	});
	   levelhiz=1500;
   }
   if(yukaricikti==100)
   {
	   level++;
	   levelo3.setVisibility(View.VISIBLE);
	     TranslateAnimation  anim = new TranslateAnimation(0,800, 0,0);
		    anim.setDuration(4000);
		    anim.setFillAfter( true );
		    levelo3.startAnimation(anim);
		    anim.setAnimationListener(new AnimationListener() {
		    	   @Override
		    	   public void onAnimationStart(Animation arg0) {
		
		    	   }
		    		   
			    	   @Override
			    	   public void onAnimationEnd(Animation arg0) {
			    		   levelo3.setVisibility(View.GONE);
		   }
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
	});
	   levelhiz=1200;
   }
   if(yukaricikti==150)
   {
	   level++;
	   levelo4.setVisibility(View.VISIBLE);
	     TranslateAnimation  anim = new TranslateAnimation(0,800, 0,0);
		    anim.setDuration(4000);
		    anim.setFillAfter( true );
		    levelo4.startAnimation(anim);
		    anim.setAnimationListener(new AnimationListener() {
		    	   @Override
		    	   public void onAnimationStart(Animation arg0) {
		
		    	   }
		    		   
			    	   @Override
			    	   public void onAnimationEnd(Animation arg0) {
			    		   levelo4.setVisibility(View.GONE);
		   }
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
	});
	   levelhiz=1000;
   }
   if(yukaricikti==200)
   {
	   level++;
	   levelo5.setVisibility(View.VISIBLE);
	     TranslateAnimation  anim = new TranslateAnimation(0,800, 0,0);
		    anim.setDuration(4000);
		    anim.setFillAfter( true );
		    levelo5.startAnimation(anim);
		    anim.setAnimationListener(new AnimationListener() {
		    	   @Override
		    	   public void onAnimationStart(Animation arg0) {
		
		    	   }
		    		   
			    	   @Override
			    	   public void onAnimationEnd(Animation arg0) {
			    		   levelo5.setVisibility(View.GONE);
		   }
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
	});
	   levelhiz=900;
   }
   if(yukaricikti==250)
   {
	   level++;
	   levelo6.setVisibility(View.VISIBLE);
	     TranslateAnimation  anim = new TranslateAnimation(0,800, 0,0);
		    anim.setDuration(4000);
		    anim.setFillAfter( true );
		    levelo6.startAnimation(anim);
		    anim.setAnimationListener(new AnimationListener() {
		    	   @Override
		    	   public void onAnimationStart(Animation arg0) {
		
		    	   }
		    		   
			    	   @Override
			    	   public void onAnimationEnd(Animation arg0) {
			    		   levelo6.setVisibility(View.GONE);
		   }
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
	});
	   levelhiz=800;
   }
 }
}
