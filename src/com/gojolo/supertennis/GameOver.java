package com.gojolo.supertennis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class GameOver extends Activity {
private InterstitialAd interstitial;
static int hight=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_over);
		AdRequest adRequest = new AdRequest.Builder().build(); 
		interstitial = new InterstitialAd(GameOver.this);
		// Insert the Ad Unit ID
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/9739341043");
		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
				// Call displayInterstitial() function
				displayInterstitial();
			}
		});
		Bundle extras = getIntent().getExtras(); 
		int id =extras.getInt("score");
		if(id>hight)
		 hight=id;
		ImageView again = (ImageView)findViewById(R.id.again);
		TextView text =(TextView)findViewById(R.id.score);
		text.setText("HIGHT SCORE:"+hight);
		again.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent  = new Intent(GameOver.this,Tennis.class);
				startActivity(intent);
				
			}
		});
	}
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
}
